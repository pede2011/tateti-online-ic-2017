//Quita advertencias
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <iostream>
#include <stdio.h>
#include <string>
#include <winsock2.h>

#pragma comment(lib,"ws2_32.lib") //Winsocket LIB

int newPort;

#define BUFLEN 512  //Maximo Largo Buffer
#define PORT newPort   //El puerto del que va a venir la info
#undef max

using namespace std;

class Datos 
{
public:
	SOCKET sckt;

	struct sockaddr_in server;
	struct sockaddr_in si_other;//Cliente que se conecta

	int slen;
	int error_handler;

	char buf[BUFLEN];
	WSADATA wsa;//Struct de WinSocket con info necesaria

	struct sockaddr_in  player1, player2;//Asigno las IPs a cada player

	int turns_given = 0;
	int grid[9];

	bool p1Turn = false;
	bool newGame = false;
};

void SetNewPort()
{
	for (;;)
	{
		cout << "Please enter PORT to use: ";
		cin >> newPort;

		if (newPort <= 49151 && newPort >= 1024 && !cin.fail())//Available port numbers per Service Name and Transport Protocol Port Number Registry
		{
			break;
		}
		
		else
		{
			cout << "Please enter a valid value" << endl;
			cin.clear();
			cin.ignore(std::numeric_limits<int>::max(), '\n');
		}					
	}
}

void InitSocket(Datos* datos)
{
	//Initialise winsock
	if (WSAStartup(MAKEWORD(2, 2), &datos->wsa) != 0)//Si no puede inicializar WinSocket
	{
		printf("Failed. Error Code : %d", WSAGetLastError());//Paso por consola el error
		exit(EXIT_FAILURE);//Cierro
	}

	printf("Socket Initialized.\n");
}

void CreateSocket(Datos* datos)
{	
	//Creo un Socket
	if ((datos->sckt = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)//Genero el socket con IPv4, Datagram Socket (sockets de UDP) y protocolo asumido de su tipo de socket
	{
		printf("Could not create socket : %d", WSAGetLastError());//Paso por consola el error
		exit(EXIT_FAILURE);
	}
	printf("Socket created.\n");
}

void PrepareSocketAddress(Datos* datos)
{
	//Prepare the sockaddr_in structure
	datos->server.sin_family = AF_INET;//Defino que las IPs a manejar son IPv4
	datos->server.sin_addr.s_addr = INADDR_ANY;
	datos->server.sin_port = htons(PORT);//Seteo cual puerto escucha el server
}

void BindSocket(Datos* datos)
{
	//Bind
	if (bind(datos->sckt, (struct sockaddr *)&datos->server, sizeof(datos->server)) == SOCKET_ERROR)//Bindeo el socket generado al socket creado previamente al inicio, el adress del server y su tama�o
	{
		printf("Bind failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	puts("Bind done\n");
}

void ClearBuffer(Datos* datos)
{
	//clear the buffer by filling null, it might have previously received data
	memset(datos->buf, '\0', BUFLEN);
}

void ListenForClient(Datos* datos)
{
	if ((datos->error_handler = recvfrom(datos->sckt, datos->buf, BUFLEN, 0, (struct sockaddr *) &datos->si_other, &datos->slen)) == SOCKET_ERROR) 
	{
		printf("recvfrom() failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
}

void ReceivePacket(Datos* datos)
{
	//try to receive some data, this is a blocking call
	if ((datos->error_handler = recvfrom(datos->sckt, datos->buf, BUFLEN, 0, (struct sockaddr *) &datos->si_other, &datos->slen)) == SOCKET_ERROR)
	{
		printf("recvfrom() failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}

	//print details of the client/peer and the data received
	printf("Received packet from %s:%d\n", inet_ntoa(datos->si_other.sin_addr), ntohs(datos->si_other.sin_port));//Aviso de que IP y Puerto recibi el paquete
	printf("Data: %s\n", datos->buf);
}

void SendData(Datos* datos, int player, string message)
{
	char msg[BUFLEN];
	strcpy_s(msg, message.c_str());

	struct sockaddr_in ip = (player == 1) ? datos->player1 : datos->player2;//Defino a que player le estoy mandando la info

	if (sendto(datos->sckt, msg, BUFLEN, 0, (struct sockaddr*) &ip, datos->slen) == SOCKET_ERROR)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
}

void PlayerHandler(Datos* datos) 
{
	printf("Waiting for players to connect...");

	ListenForClient(datos);

	if (inet_ntoa(datos->player1.sin_addr)[0] != '1' && inet_ntoa(datos->player1.sin_addr)[1] != '2')
	{
		datos->player1 = datos->si_other;		
		printf("\n Player 1 has connected to server \n");
	}
	else 
	{
		datos->player2 = datos->si_other;		
		printf("\n Player 2 has connected to server \n");
	}
}

void SelectStartingPlayer(Datos* datos) //Gracias Ferra por el Random Selecter
{

	//Sol�a ser un Random select de los jugadores, pero confundia m�s que otra cosa
/*	if (rand() % 10 + 1 == 0) 
	{
		datos->p1Turn = true;
		SendData(datos, 1, "1");
		SendData(datos, 2, "2");
	}
	else 
	{
		SendData(datos, 1, "2");
		SendData(datos, 2, "1");
	}*/

	datos->p1Turn = true;
	SendData(datos, 1, "1");
	SendData(datos, 2, "2");
}

void DisplayGrid(Datos* datos)//Gracias Google por una grilla que no es imposible de entender
{	
	cout << endl << datos->grid[0] << "|" << datos->grid[1] << "|" << datos->grid[2] << endl;
	cout << datos->grid[3] << "|" << datos->grid[4] << "|" << datos->grid[5] << endl;
	cout << datos->grid[6] << "|" << datos->grid[7] << "|" << datos->grid[8] << endl << endl;
}

void ClearGrid(Datos* datos) 
{
	for (int i = 0; i < 10; i++)
	{
		datos->grid[i] = 0;
	}
}

bool CantMove(Datos* datos)
{
	for (int i = 0; i < 9; i++) 
	{
		if (datos->grid[i] == 0)
		{
			return false;
		}
	}

	return true;
}

void Restart(Datos* datos) 
{
	ClearGrid(datos);
	datos->p1Turn = false;
	datos->turns_given = 0;
	datos->newGame = true;

	printf("\nGame is being restarted\n");
}

void CheckTie(Datos* datos) 
{
	SendData(datos, 1, "TIE");
	SendData(datos, 2, "TIE");
	Restart(datos);
}

void CheckGameOver(Datos* datos) 
{
	if (CantMove(datos)==true)
	{
		CheckTie(datos);
		return;
	}

	//Gracias Google por los checks para ver quien gano

	int winner = -1;

	if (datos->grid[0] == datos->grid[1] && datos->grid[0] == datos->grid[2] && datos->grid[0] != 0) {
		winner = datos->grid[0];
	}

	if (datos->grid[0] == datos->grid[4] && datos->grid[0] == datos->grid[8] && datos->grid[0] != 0) {
		winner = datos->grid[0];
	}

	if (datos->grid[0] == datos->grid[3] && datos->grid[0] == datos->grid[6] && datos->grid[0] != 0) {
		winner = datos->grid[0];
	}

	if (datos->grid[1] == datos->grid[4] && datos->grid[1] == datos->grid[7] && datos->grid[1] != 0) {
		winner = datos->grid[1];
	}

	if (datos->grid[2] == datos->grid[4] && datos->grid[2] == datos->grid[6] && datos->grid[2] != 0) {
		winner = datos->grid[2];
	}

	if (datos->grid[2] == datos->grid[5] && datos->grid[2] == datos->grid[8] && datos->grid[2] != 0) {
		winner = datos->grid[2];
	}

	if (datos->grid[3] == datos->grid[4] && datos->grid[3] == datos->grid[5] && datos->grid[3] != 0) {
		winner = datos->grid[3];
	}

	if (datos->grid[6] == datos->grid[7] && datos->grid[6] == datos->grid[8] && datos->grid[6] != 0) {
		winner = datos->grid[6];
	}

	if (winner == 1) 
	{
		SendData(datos, 1, "WIN");
		SendData(datos, 2, "LOSE");
		Restart(datos);
	}

	if (winner == 2) 
	{
		SendData(datos, 2, "WIN");
		SendData(datos, 1, "LOSE");
		Restart(datos);
	}

	if (winner == -1) //Esto se hace para mantener el flujo de datos durante turnos, incluso si no hay un ganador
	{
		SendData(datos, 1, "RUNNING");
		SendData(datos, 2, "RUNNING");
	}
}

void ListenForAction(Datos* datos) 
{
	DisplayGrid(datos);

	ListenForClient(datos);

	string tmpBuffer = datos->buf;
	if (tmpBuffer == "NEED TURN")
	{
		if (datos->turns_given == 0)
		{
			datos->turns_given = 1;
		}
		else 
		{
			SendData(datos, 1, "1");
			SendData(datos, 2, "1");

			datos->turns_given = 0;
		}
	}

	if (tmpBuffer == "1" || tmpBuffer == "2" || tmpBuffer == "3" || tmpBuffer == "4" || tmpBuffer == "5" || tmpBuffer == "6" || tmpBuffer == "7" || tmpBuffer == "8" || tmpBuffer == "9")
	{
		if (datos->p1Turn) 
		{			
			datos->grid[stoi(tmpBuffer) - 1] = 1;
			datos->p1Turn = false;
		}
		else 
		{
			datos->grid[stoi(tmpBuffer) - 1] = 2;
			datos->p1Turn = true;
		}
		
		SendData(datos, 1, tmpBuffer);
		SendData(datos, 2, tmpBuffer);

		CheckGameOver(datos);
	}
}

int main()
{

	Datos* datos = new Datos();

	SetNewPort();
	
	datos->slen = sizeof(datos->si_other);
	
	ClearGrid(datos);
	
	InitSocket(datos);
	
	CreateSocket(datos);
	
	PrepareSocketAddress(datos);
	
	BindSocket(datos);	

	ClearBuffer(datos);

	PlayerHandler(datos);
	PlayerHandler(datos);

	SelectStartingPlayer(datos);

	//keep listening for data
	while (1)//Mientras return devuelva 1, o sea, hasta que cierre el server
	{
		if (datos->newGame) 
		{
			SelectStartingPlayer(datos);

			datos->newGame = false;
		}

		ClearBuffer(datos);

		ListenForAction(datos);

		if (CTRL_CLOSE_EVENT == true)
		{
			closesocket(datos->sckt);
			WSACleanup();

			return 0;
		}
	}

	closesocket(datos->sckt);
	WSACleanup();

	return 0;
}