#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma warning(disable : 4996) 

#include<stdio.h>
#include <iostream>
#include <string>
#include<winsock2.h>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

std::string serverIPString;
int newPort;

#define SERVER serverIPString.c_str() //ip address of udp server
#define BUFLEN 512  //Max length of buffer
#define PORT newPort   //The port on which to listen for incoming data
#undef max

using namespace std;

class Datos
{
public:
	struct sockaddr_in si_other;
	int s;
	int slen;
	char buf[BUFLEN];
	char message[BUFLEN];
	WSADATA wsa;

	int playerNumber;
	int grid[9];
	bool yourTurn = false;
	bool newGame = false;
};

bool CheckIPInput(char* ipAddress)
{
	while (*ipAddress)
	{
		if (*ipAddress >= '0' && *ipAddress <= '9')
		{
			++ipAddress;
		}		
		else
		{
			return 0;
		}
	}
	return 1;
}

bool CheckIPValidity(const char* serverIPToCheck)
{
	char* ipAddress = (char*)serverIPToCheck;

	int i, num, dots = 0;
	char *ptr;
	
	if (ipAddress == NULL)
	{
		return 0;
	}
	
	ptr = strtok(ipAddress, ".");//Breaks the IP Address by the DOT
	
	if (ptr == NULL)//If there is no input
	{
		return 0;
	}
	
	while (ptr)
	{
		if (!CheckIPInput(ptr))//If the IP includes anything but a number
		{
			return 0;
		}

			num = atoi(ptr);//Parses the string by the dots, and gets rid of all the whitespaces

			if (num >= 0 && num <= 255)//ATOI gives back a number between 0 and 255, which is the sum of all the ints it finds between dots
			{
				//If there is ANYTHING (which by this point is a group of ints), add a dot after a set interval
				ptr = strtok(NULL, ".");
				if (ptr != NULL)
				{
					++dots;
				}
			
			}
			else
			{
				return 0;
			}
	}
			

	if (dots != 3)//If the IP is longer than 0.0.0.0
	{
		return 0;
	}
		return 1;
}


void SetNewIP()
{
	for (;;)
	{
		cout << "Please enter IP of the server: ";
		string serverIP;

		cin >> serverIP;

		//serverIP[strlen(serverIP.c_str()) - 1] = 0;//gets rid of the /n. NO LONGER IN USE
	
		serverIPString = serverIP;

		if (CheckIPValidity(serverIP.c_str())==true)
		{
			break;
		}
		else
		{
			cout << "Please enter a valid IP Address" << endl;
			cin.clear();
		}
		
	}
}


void SetNewPort()
{
	for (;;)
	{
		cout << "Please enter PORT to use: ";
		cin >> newPort;
		if (newPort <= 49151 && newPort >= 1024 && !cin.fail())//Available port numbers per Service Name and Transport Protocol Port Number Registry
		{
			break;
		}
		else
		{
			cout << "Please enter a valid value" << endl;
			cin.clear();
			cin.ignore(std::numeric_limits<int>::max(), '\n');
		}
	}
}

void InitSocket(Datos* datos)
{
	if (WSAStartup(MAKEWORD(2, 2), &datos->wsa) != 0)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	printf("Socket Initialized.\n");	
}

void CreateSocket(Datos* datos)
{
	if ((datos->s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
	{
		printf("socket() failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}

	printf("Socket Created\n");
}

void SetupAdress(Datos* datos)
{
	memset((char *)&datos->si_other, 0, sizeof(datos->si_other));
	datos->si_other.sin_family = AF_INET;
	datos->si_other.sin_port = htons(PORT);
	datos->si_other.sin_addr.S_un.S_addr = inet_addr(SERVER);
}

void SendData(Datos* datos)
{
	if (sendto(datos->s, datos->message, strlen(datos->message), 0, (struct sockaddr *) &datos->si_other, datos->slen) == SOCKET_ERROR)
	{
		cout << "Error: " << WSAGetLastError() << endl;
		exit(EXIT_FAILURE);
	}
}

void ClearBuffer(Datos* datos)
{
	memset(datos->buf, '\0', BUFLEN);
}

void ListenForServer(Datos* datos)
{
	if (recvfrom(datos->s, datos->buf, BUFLEN, 0, (struct sockaddr *) &datos->si_other, &datos->slen) == SOCKET_ERROR)
	{
		printf("recvfrom() failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
}

void SendInitHandshake(Datos* datos) 
{
	strcpy_s(datos->message, "Handshake");
	SendData(datos);
	ClearBuffer(datos);
}

void GetPlayerNumber(Datos* datos)
{
	printf("\nWaiting for other players...");

	ListenForServer(datos);

	if (datos->buf[0] == '1') 
	{
		datos->playerNumber = 1;
		printf("\nYou are now Player 1\n");
	}
	else
	{
		printf("\nYou are now Player 2\n");
		datos->playerNumber = 2;
	}
}

void SetGrid(Datos* datos, int play, int player)
{
	datos->grid[play - 1] = player;
}

bool CheckInput(Datos* datos, string msg) 
{
	if (msg == "1" || msg == "2" || msg == "3" || msg == "4" || msg == "5" || msg == "6" || msg == "7" || msg == "8" || msg == "9") 
	{
		if (datos->grid[std::stoi(msg) - 1] == 0) 
		{
			return true;
		}
		else 
		{
			printf("\nSpace already taken, please try again...\n\n");
			return false;
		}
	}
	else
	{
		printf("\nInvalid input, please try again\n\n");
		return false;
	}
}

void PlayTurn(Datos* datos)
{
	ClearBuffer(datos);
	bool playAvailable = false;	

	while (!playAvailable)
	{
		printf("Enter movement (1-9): ");
		gets_s(datos->message);
		string tmpMSG = datos->message;
		if (CheckInput(datos, tmpMSG))
		{
			playAvailable = true;
		}				
	}

	string tmpMSG = datos->message;
	SetGrid(datos, stoi(tmpMSG), 1);
	SendData(datos);
	ClearBuffer(datos);
}

int GetPlayerTurn(Datos* datos)
{
	strcpy_s(datos->message, "NEED TURN");
	SendData(datos);
	ClearBuffer(datos);
	ListenForServer(datos);

	return stoi(datos->buf);
}

void WaitForTurn(Datos* datos)//Escucho pero no escribo, hasta que cambie la grilla, o sea, hasta que termine el turno del otro
{
	ClearBuffer(datos);

	ListenForServer(datos);

	string tmpBuffer = datos->buf;
	SetGrid(datos, stoi(tmpBuffer), 2);
}

void ClearGrid(Datos* datos)
{
	for (int i = 0; i < 10; i++)
	{
		datos->grid[i] = 0;
	}
}

void DisplayGrid(Datos* datos)
{
	cout << endl << datos->grid[0] << "|" << datos->grid[1] << "|" << datos->grid[2] << endl;
	cout << datos->grid[3] << "|" << datos->grid[4] << "|" << datos->grid[5] << endl;
	cout << datos->grid[6] << "|" << datos->grid[7] << "|" << datos->grid[8] << endl << endl;
}

void RestartGame(Datos* datos)
{
	ClearGrid(datos);
	datos->yourTurn = false;
	datos->newGame = true;
}

void CheckIfGameOver(Datos* datos)
{
	ClearBuffer(datos);
	ListenForServer(datos);

	string tmpBuffer = datos->buf;

	if (tmpBuffer == "TIE")
	{
		DisplayGrid(datos);
		printf("\nIT'S A TIE. IT'S A TIE. IT'S A TIE. IT'S A TIE. IT'S A TIE. \n");
		RestartGame(datos);
	}

	if (tmpBuffer == "WIN")
	{
		DisplayGrid(datos);
		printf("\nYOU WON. YOU WON. YOU WON. YOU WON. YOU WON. YOU WON. YOU WON. YOU WON.\n");
		RestartGame(datos);
	}

	if (tmpBuffer == "LOSE")
	{
		DisplayGrid(datos);
		printf("\nYOU LOST. YOU LOST. YOU LOST. YOU LOST. YOU LOST. YOU LOST. YOU LOST.\n");
		RestartGame(datos);
	}
}

int main(void) {

	//*****
	//Start
	//*****
	Datos* datos = new Datos();

	SetNewIP();

	SetNewPort();

	datos->slen = sizeof(datos->si_other);//Tengo que hacerlo aca porque sino jode

	ClearGrid(datos);//Limpio la grilla

	InitSocket(datos); //Inicio WinSocket

	CreateSocket(datos);//Creo el Socket

	SetupAdress(datos);//Le paso IP y Port al server

	ClearBuffer(datos);//Limpio el buffer de comunicación

	SendInitHandshake(datos);//Inicio comunicación

	printf("Connection to server done\n");

	GetPlayerNumber(datos);//Asigno slots

	if (GetPlayerTurn(datos) == datos->playerNumber)//Habilito cuando es el turno de cada uno. Esto lo hace el server en realidad
	{
		datos->yourTurn = true;
	}
	//******
	//Update
	//******
	while (1)
	{
		if (datos->newGame)//Para iniciar de 0
		{
			GetPlayerNumber(datos);

			if (GetPlayerTurn(datos) == datos->playerNumber) 
			{
				datos->yourTurn = true;
			}

			datos->newGame = false;
		}

		DisplayGrid(datos);//Muestro tablero en pantalla

		if (datos->yourTurn)//Si es tu turno
		{	
			printf("Your turn:\n");
			PlayTurn(datos);
			ClearBuffer(datos);
			ListenForServer(datos);
			datos->yourTurn = false;
		}
		else //Mientras no es tu turno
		{		
			printf("Opponent's turn, please wait...");
			WaitForTurn(datos);
			datos->yourTurn = true;
		}		
		CheckIfGameOver(datos);//Reviso si termino la partida. La confirmacion la recibo del server

		if (CTRL_CLOSE_EVENT == true)
		{
			closesocket(datos->s);
			WSACleanup();

			return 0;
		}
	}
	//Cierro todo
	closesocket(datos->s);
	WSACleanup();

	return 0;
}